<?php

namespace App\Docsets;

use Godbout\DashDocsetBuilder\Docsets\BaseDocset;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Wa72\HtmlPageDom\HtmlPageCrawler;

class GuixDocset extends BaseDocset
{
    public const CODE = 'guix';
    public const NAME = 'GNU Guix';
    public const URL = 'guix.gnu.org';
    public const INDEX = 'en/manual/en/html_node/index.html';
    public const PLAYGROUND = '';
    public const ICON_16 = 'static/base/img/icon.png';
    public const ICON_32 = 'static/base/img/icon.png';
    public const EXTERNAL_DOMAINS = [];

    public function grab(): bool
    {
        system(
            "echo; wget https://guix.gnu.org/en/manual/en/html_node/ \
                --trust-server-names \
                -r \
                --no-parent \
                --page-requisites \
                --adjust-extension \
                --convert-links \
                --domains={$this->externalDomains()} \
                --directory-prefix=storage/{$this->downloadedDirectory()} \
                -e robots=off \
                --quiet \
                --show-progress",
            $result
        );

        return $result === 0;
    }


    public function entries(string $file): Collection
    {
        $crawler = HtmlPageCrawler::create(Storage::get($file));

        $entries = collect();

        $entries = $entries->union($this->setupEntries($crawler, $file));

        return $entries;
    }

    protected function setupEntries(HtmlPageCrawler $crawler, string $file)
    {
        $entries = collect();

        $crawler->each(function (HtmlPageCrawler $node) use ($entries, $file) {
            preg_match('/([\w\s]*)(\(.*\)).*/', $node->text(), $matches);
            $title = $matches[1];
            $type = "Guide";
            if (Str::contains($title, "Invoking")) {
                $type = "Commands";
            }
            if (Str::contains($title, "Reference")) {
                $type = "Class";
            }
            $entries->push([
                'name' =>  $title, //$this->cleanAnchorText($node->text()),
                'type' => $type,
                'path' => Str::after($file . '#' . Str::slug($node->text()), $this->innerDirectory())
            ]);
        });

        return $entries;
    }

    public function format(string $file): string
    {
        $crawler = HtmlPageCrawler::create(Storage::get($file));

        //

        return $crawler->saveHTML();
    }

    protected function cleanAnchorText($anchorText)
    {
        return trim(preg_replace('/\s+/', ' ', $anchorText));
    }
}
