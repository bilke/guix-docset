# GNU Guix Dash Docset

## Setup

```bash
composer install
export PATH=$PWD/vendor/bin:$PATH

dash-docset build guix-docset

# skip download:
dash-docset package guix-docset
```
